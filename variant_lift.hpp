#pragma once
#include <variant>
#include "meta_utils.hpp"

namespace result::utils {
using result::meta::type_list;
template < typename... Types1, template <typename...> class V1
         , typename... Types2, template <typename...> class V2
         >
bool lift_into(V1<Types1...> source, V2<Types2...>& target )
{
  static_assert(type_list<Types2...>::template contains_subset<Types1...>::value, "Types of source must be a subset of target types!");
  using std::get;
  using std::holds_alternative;
  return (... || ((holds_alternative<Types1>(source)) ?  target = get<Types1>(std::move(source)), true
                                                            : false));
}

template < typename... Types1, template <typename...> class V1
         , typename... Types2, template <typename...> class V2
         , typename... List
         >
bool lift_into(V1<Types1...>&& source, V2<Types2...>& target, [[maybe_unused]] type_list<List...> reified_list )
{
  static_assert(type_list<Types1...>::template contains_subset<List...>::value, "Lifted types must be in source");
  static_assert(type_list<Types2...>::template contains_subset<List...>::value, "Lifted types must be in target");
  using std::get;
  using std::holds_alternative;
  return (... || ((holds_alternative<List>(source)) ?  target = get<List>(std::move(source)), true
                                                            : false));
}
}
