// TODO:  https://godbolt.org/z/GbbjrL https://godbolt.org/z/lOMLuE
#include <cmath>
#include <limits>
#include <memory>
#include <string>
#include <string_view>
#include <cstdio>
#include <cassert>

#include "meta_utils.hpp"
#include "variant_lift.hpp"
#include "result.hpp"
#include "switch.hpp"

using namespace result; // it's just an example

struct SqrtOfNegativeNumber{
  int error_number;
};
auto my_sqrt(int i) -> Result<float, SqrtOfNegativeNumber>  {
  if (i < 0) {
    return SqrtOfNegativeNumber {i};
  }
  return std::sqrt(float(i));
}


struct NumberTooLargeForInt {
  float big_number;
};
auto ceil_int(float f) -> Result<int, NumberTooLargeForInt> {
  if (std::numeric_limits<int>::max() <= f) {
    return NumberTooLargeForInt{f};
  }
  return  static_cast<int>(std::ceil(f));
}


struct IntTooSmall {
  std::string msg;
};
auto integer_sink(int i) -> Result<void, IntTooSmall> {
  if(i < 42) {
    return  IntTooSmall {std::string {"Integer was too large!"}};
  } else {
    return {};
  }
}

struct Printer {
  Result<int> say_hi() { std::puts("said hi\n"); return 0;}
};

template <typename T>
auto print(T t) -> Result<Printer> {std::printf("%d\n", t); return Printer{};}

int complicated_computation(int i) {return i*3+5;}

int main(int argc, [[maybe_unused]] char* argv[]) {
  Result<int> res(argc - 2); 
  std::function<Result<int>(int)> t = [](int i) {return Result<int>{i+i};};
  auto new_res = res
    .then(my_sqrt)
    .then(ceil_int)
    .then(t)
    .then([](int i) {return i-1;})
    .then([](int i) {return Result<int>{i+i};})
    .then(complicated_computation)
    .then(print<int>) // requires explicit specialization
    .then(&Printer::say_hi);
    ;
  static_assert(std::is_same_v<decltype(new_res), Result<int, NumberTooLargeForInt, SqrtOfNegativeNumber>>);

  Switch(new_res)
    .Case<SqrtOfNegativeNumber>([](const SqrtOfNegativeNumber& e) {std::printf("Passed number %d is negative!\n", e.error_number);})
    .Case<NumberTooLargeForInt>([]() {std::printf("Hello, Error!\n");})
    | ESAC; 

  Result<std::unique_ptr<int>> mov_only_res = std::make_unique<int>();
  int the_anwser = 42;
  auto new_mov_only_res = mov_only_res
    .then([&the_anwser](std::unique_ptr<int> iptr){ *iptr = the_anwser; return iptr; })
    .then([&the_anwser](std::unique_ptr<int> iptr){ *iptr = the_anwser; return Result<std::unique_ptr<int>> {std::move(iptr)}; })
    .then([](std::unique_ptr<int> ) -> Result<std::unique_ptr<float>, std::string>{
        using namespace std::string_literals;
        return Result<std::unique_ptr<float>, std::string> {"Expected error occured!"s}; 
        })
    ;

  Switch(std::move(new_mov_only_res))
    .Case<std::string>([](std::string_view s){ std::printf("%s\n", s.data()); })
    | ESAC;

  auto questionable_error_res = Result<int,  std::unique_ptr<char*>>{2};
  questionable_error_res
    .then([](int i){return i;})
    ;

  assert(questionable_error_res && questionable_error_res.unsafe_get_result() == 2);

  Switch(std::move(questionable_error_res))
    .Case<std::unique_ptr<char*>>([]() {std::puts("You should not see this message if result works correctly");})
    | ESAC;

  Result<void, int, float> void_result;
  // what
  //   .then([](){ // Will cause a compile error
  //     using namespace std::string_literals;
  //     return "This is unexpected"s;
  //   })
  //  ;
  Switch(void_result);
  Result<void, int, float, std::unique_ptr<int>> void_result2 = {2};
  Switch(std::move(void_result2))
    .Case<int>([](){std::puts("should trigger");})
    .Case<float>([](){std::puts("should not be visible");})
    .Case<std::unique_ptr<int>>([](){std::puts("should not be visible");})
    |ESAC
    ;

  auto void_result3 = Result<int, SqrtOfNegativeNumber>{1}
    .then([](int){})
      ;
  Switch(void_result3)
    .Case<SqrtOfNegativeNumber>([](){std::puts("This should not happen");})
    |ESAC;
    ;

  auto void_result4 = Result<int>{} 
    .then(integer_sink)
    ;
  Switch(void_result4)
    // .Case<IntTooSmall>([](int e){})
    .Case<IntTooSmall>([](auto e){std::printf("An expected error: %s\n", e.msg.data());})
    |ESAC;
    ;

  assert(Result<int>{}.unsafe_get_result() == 0);
  assert(Result<std::string>{}.unsafe_get_result() == "");
  assert(!std::move(Result<std::unique_ptr<int>>{}).unsafe_get_result());


  Result<int> bind_test(argc - 2); 
  auto new_res_bind = bind_test
     >> my_sqrt
     >> ceil_int
     >> t
     >> [](int i) {return i-1;}
     >> [](int i) {return Result<int>{i+i};}
     >> complicated_computation
     >> print<int> // requires explicit specialization
     >> &Printer::say_hi
    ;
  static_assert(std::is_same_v<decltype(new_res_bind), Result<int, NumberTooLargeForInt, SqrtOfNegativeNumber>>);

  {
    Result<std::unique_ptr<int>> mov_only_res = std::make_unique<int>();
    int the_anwser = 42;
    auto new_mov_only_res = mov_only_res
      >> [&the_anwser](std::unique_ptr<int> iptr){ *iptr = the_anwser; return iptr; }
      >> [&the_anwser](std::unique_ptr<int> iptr){ *iptr = the_anwser; return Result<std::unique_ptr<int>> {std::move(iptr)}; }
      >> [](std::unique_ptr<int> ) -> Result<std::unique_ptr<float>, std::string>{
          using namespace std::string_literals;
          return Result<std::unique_ptr<float>, std::string> {"Expected error occured!"s}; 
          }
    ;

    Switch(std::move(new_mov_only_res))
      .Case<std::string>([](std::string_view s){ std::printf("%s\n", s.data()); })
      | ESAC;
  }

  {
    Switch(Result<int>{})|ESAC;
  }

  // Enable this block when same type for error and result is supported
  // {
  //   auto res = Result<std::string, std::string>(std::string{"should be printed"});
  //   auto new_res = res.then([](std::string_view str){
  //       std::printf("%s\n", str.data());
  //       return std::string("should not be printed");
  //       });
  //   Switch(res)
  //     .Case<std::string>([](std::string_view str) {std::printf("%s\n", str.data());})
  //     |ESAC;
  // }
  {
    Result<int, float, bool> res1 {};
    Result<char, short, bool> res2 {};
    Result<char, short, bool> res3 {};
    Result<bool> res4 {};
    auto combined = (res1, res2, res3, res4);
    static_assert(std::is_same_v<decltype(combined), Result<std::tuple<int, char, char, bool>, float, bool, short>>);
  }
  {
    Result<std::string, short> res1 = std::string("Hello world");
    Result<char, int> res2 = '!';
    std::function<bool(std::string, char)> f = [](std::string msg, char punctuation) {
      std::printf("%s%c\n", msg.data(), punctuation);
      return true;
    };
    auto func_res =  f | (res1, res2);
    static_assert(std::is_same_v<decltype(func_res), Result<bool, short, int>>);
  }
  {
    Result<std::string, short> res1 = std::string("Hello world");
    Result<char, int> res2 = '!';
    auto func_res = [](std::string msg, char punctuation) {
      std::printf("%s%c\n", msg.data(), punctuation);
    } | (res1, res2);
    static_assert(std::is_same_v<decltype(func_res), Result<void, short, int>>);
  }
  {
    Result<std::string, short> res1 = std::string("Hello world");
    Result<char, int> res2 = '!';
    std::function<Result<size_t>(std::string, char)> f = [](std::string msg, char punctuation) {
      std::printf("%s%c\n", msg.data(), punctuation);
      return Result<size_t>{msg.length()};
    };
    auto func_res =  f | (res1, res2);
    static_assert(std::is_same_v<decltype(func_res), Result<size_t, short, int>>);
  }

  return 0;
}
