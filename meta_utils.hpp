#pragma once
#include <type_traits>
#include <utility>

namespace result::meta {

struct invalid_head {};
template <typename ...P> struct type_list
{
    template <template <typename...> typename T> using apply = T<P...>;

    template <typename... Ts> using prepend = type_list<Ts..., P...>;
    template <typename... Ts> using append = type_list<P...,Ts...>;

    template <typename T> using has_member = std::integral_constant<bool, (... || std::is_same_v<T, P>)>;
#if defined(__clang__)
    template <typename ...Ts> using contains_subset = std::integral_constant<bool, (... && has_member<Ts>::value)>;
#else
    // All compilers but clang seem to hate using fold expressions in using type aliases...
  private:
    template <typename T, typename ...Ts> struct subset_h {
      using result = std::integral_constant<bool, (... && T::template has_member<Ts>::value)>;
    };
  public:
    template <typename ...Ts> using contains_subset = typename subset_h<type_list<P...>, Ts...>::result;
#endif

  private:
    template< typename Head, typename... Tail>  struct splitter {
      using head = Head;
      using tail = type_list<Tail...>;
    };

    template<typename HasHead, typename... Ts> struct split_helper {
      using head = invalid_head;
      using tail = type_list<Ts...>;      
    };
    
    template<typename... Ts> struct split_helper<std::true_type, Ts...> : splitter<Ts...> {};
    static constexpr bool has_head = sizeof...(P);
  public:
    using head = typename split_helper<std::bool_constant<has_head>, P...>::head;
    using tail = typename split_helper<std::bool_constant<has_head>, P...>::tail;


  private:
    template <typename Head, typename Tag, typename T, typename Tail> struct remover_h;
    template <typename Tag, typename T, typename Head, typename Tail> struct remover
    {
      using result =  typename remover_h<typename Tail::head, typename std::is_same<T, typename Tail::head>::type, T, typename Tail::tail>::result;
    };
    template <typename T, typename Head, typename Tail> struct remover<std::false_type, T, Head, Tail>
    {
      using result =  typename remover_h<typename Tail::head, typename std::is_same<T, typename Tail::head>::type, T, typename Tail::tail>::result::template prepend<Head>;
    };

    template <typename Head, typename Tag, typename T, typename Tail>
    struct remover_h
    {
      using result = typename remover<Tag, T, Head, Tail>::result;
    };
    template <typename Tag, typename T, typename Tail>
    struct remover_h<invalid_head, Tag, T, Tail> {
      using result = type_list<>;
    };
  public:
    template<typename T> using remove_all = typename remover<std::true_type::type, T, void, type_list<P...> >::result;
  private:
    template<typename Head, typename Tail> struct unique_h { using result = typename Tail::template remove_all<Head>::unique::template prepend<Head>; };
    template<typename Discard> struct unique_h<invalid_head, Discard> { using result = type_list<>; };
  public:
    using unique = typename unique_h<head, tail>::result;

    constexpr static auto are_unhandled_cases = std::is_same_v<head, invalid_head>;
};

namespace type_list_tests {

static_assert(type_list<int, float, bool>::has_member<float>::value);
static_assert(type_list<int, float, bool>::contains_subset<float, int>::value);
static_assert(std::is_same_v<type_list<int, float, bool>::head, int>);
static_assert(std::is_same_v<type_list<int, float, bool>::tail, type_list<float, bool>>);
static_assert(std::is_same_v<type_list<>::tail, type_list<>>);
static_assert(std::is_same_v<type_list<int>::remove_all<int>, type_list<>>);
static_assert(std::is_same_v<type_list<int,int>::remove_all<int>, type_list<>>);
static_assert(std::is_same_v<type_list<float>::remove_all<int>, type_list<float>>);
static_assert(std::is_same_v<type_list<int, bool, float, int>::remove_all<int>, type_list<bool, float>>);
static_assert(std::is_same_v<type_list<int, int>::unique, type_list<int>>);
static_assert(std::is_same_v<type_list<int, int, float, float, float>::unique, type_list<int, float>>);

}

}// end namespace result::meta
