#pragma once
#include <type_traits>
#include <variant>
#include <utility>
#include <functional>

#include "result.hpp"

namespace result {

template<typename ResultTypes, typename UnhandledErrorTypes>
class Switcher
{
  typename ResultTypes::template apply<std::variant> m_errors;

  template<typename Okay, typename ...Errors>
  friend auto Switch(Result<Okay, Errors...> result) -> std::enable_if_t<
    std::is_copy_constructible_v<Result<Okay, Errors...>>,
    Switcher<meta::type_list<detail::Wrapper<Okay>, Errors...>, meta::type_list<Errors...>>
  >;


  template<typename Okay, typename ...Errors>
  friend auto Switch(Result<Okay, Errors...>&& result) -> std::enable_if_t<
    !std::is_copy_constructible_v<Result<detail::Wrapper<Okay>, Errors...>>,
    Switcher<meta::type_list<Okay, Errors...>, meta::type_list<Errors...>>
  >;

  template<typename Okay, typename ...Errors>
  Switcher(Result<Okay, Errors...>&& result) : m_errors(std::move(result.m_result)) {}

  public: // workaround
  Switcher(decltype(m_errors) variant)
    : m_errors(std::move(variant)) {}

  template<typename Error, typename F>
  auto Case(F&& f) -> std::enable_if_t<UnhandledErrorTypes::template has_member<Error>::value,
       Switcher<ResultTypes, typename UnhandledErrorTypes::template remove_all<Error>>
         > {
    if (std::holds_alternative<Error>(m_errors)) {
      if constexpr(std::is_invocable_v<F, Error>) {
        std::invoke(f, std::get<Error>(std::move(m_errors)));
      } else {
        static_assert(std::is_invocable_v<F>, "Function passed to case must either accept type Error or take no arguments at all!");
        f();
      }
    }
    return std::move(m_errors);
  }

};

constexpr struct Esac {} ESAC;


template<typename Ignored, typename T>
  void operator|(Switcher<Ignored, T>&&, Esac) {
    static_assert(T::are_unhandled_cases , "Did not cover all cases");
  }

template<typename Okay, typename ...Errors>
auto Switch(Result<Okay, Errors...> result) -> std::enable_if_t<
  std::is_copy_constructible_v<Result<Okay, Errors...>>,
  Switcher<meta::type_list<detail::Wrapper<Okay>, Errors...>, meta::type_list<Errors...>>
  >
{
  return {std::move(result)};
}

template<typename Okay, typename ...Errors>
auto Switch(Result<Okay, Errors...>&& result) -> std::enable_if_t<
  !std::is_copy_constructible_v<Result<Okay, Errors...>>,
  Switcher<meta::type_list<detail::Wrapper<Okay>, Errors...>, meta::type_list<Errors...>>
  >
{
  return {result};
}

template<typename ...Errors>
auto Switch(Result<void, Errors...> result)
{
  return Switch( std::forward<Result<detail::Wrapper<void>, Errors...>>(result) );
}
}
