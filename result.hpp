#pragma once
#include <type_traits>
#include <variant>
#include <utility>
#include <functional>
#include <tuple>

namespace result {

template <typename Okay, typename ...Errors>
class Result;

template<typename T>
struct is_result : std::false_type {};

template <typename Okay, typename ...Errors>
struct is_result<Result<Okay, Errors...>> : std::true_type {};

namespace detail {
template<typename T>
struct Wrapper {
#if __has_cpp_attribute(no_unique_address)
  [[no_unique_address]] T unwrap;
#else
  T unwrap;
#endif
};

template<>
struct Wrapper<void> {};
}

// taken from https://stackoverflow.com/a/48458312/1078224
template <typename> struct is_tuple: std::false_type {};
template <typename ...T> struct is_tuple<std::tuple<T...>>: std::true_type {};

template <typename Okay, typename ...Errors>
class Result {
  public:
  using okay_t = detail::Wrapper<Okay>;
  using okay_unwrapped_t = Okay;
  using err_t = meta::type_list<Errors...>;
  static_assert(!err_t::template has_member<Okay>::value, "Result currently does not support having the same type as the okay type and as an error type");
  using var_t = std::variant<okay_t, Errors...>;
  template<typename ResultTypes, typename ErrorList>
  friend class Switcher;

  auto unsafe_get_result() -> Okay {return std::get<okay_t>(std::move(m_result)).unwrap;}
  auto to_variant() -> var_t {return std::move(m_result);}

  template<typename T>
  Result(T&& t) : m_result(std::forward<T>(t)) {}
  Result(Okay&& ok) : m_result(detail::Wrapper<Okay> {std::forward<Okay>(ok)}) {}


  Result() = default;
  Result(Result &) = default;
  Result(Result &&) = default;
  Result& operator=(Result&) = default;
  Result& operator=(Result&&) = default;

  constexpr operator bool() {
    using std::holds_alternative;
    return holds_alternative<okay_t>(m_result);
  }

  template<typename F>
  auto then(F&& f) -> std::enable_if_t<
    std::is_invocable_v<std::decay_t<F>, Okay> && is_result<std::invoke_result_t<std::decay_t<F>, Okay>>(),
    typename std::invoke_result_t<std::decay_t<F>, Okay>::err_t::template append<Errors...>::template prepend<typename std::invoke_result_t<F,Okay>::okay_unwrapped_t>::unique ::template apply<Result>
  >
  {
    typename decltype(then(f))::var_t new_variant;
    using result::utils::lift_into;
    if (std::holds_alternative<okay_t>(m_result)) {
      lift_into(std::invoke(f, std::get<okay_t>(std::move(m_result)).unwrap).to_variant(), new_variant);
    } else {
      lift_into(std::move(m_result), new_variant, meta::type_list<Errors...> {});
    }
    return new_variant;
  }

  template<typename F>
  auto then(F&& f) -> std::enable_if_t<
    std::is_invocable_v<F, Okay> && !is_result<std::invoke_result_t<F, Okay>>(),
    Result<std::invoke_result_t<F, Okay>, Errors...> 
  >
  {
    typename decltype(then(f))::var_t new_variant;
    if (std::holds_alternative<okay_t>(m_result)) {
      if constexpr(std::is_same_v<std::invoke_result_t<F, Okay>, void>) {
        return {detail::Wrapper<void>{}};
      } else {
        new_variant = detail::Wrapper<std::invoke_result_t<F, Okay>> { std::invoke(f, std::get<okay_t>(std::move(m_result)).unwrap) };
      }
    } else {
      using result::utils::lift_into;
      lift_into(std::move(m_result), new_variant, meta::type_list<Errors...> {});
    }
    return new_variant;
  }


  template<typename F>
  auto operator>>(F&& f) -> std::enable_if_t<
    std::is_invocable_v<F, Okay>,
    decltype(this->then(f))
  >
  {
    return this->then(f);
  }

  template<typename Okay2, typename... Errors2>
  auto operator,(Result<Okay2, Errors2...>& next_res) -> std::enable_if_t<
    not is_tuple<Okay>::value,
    typename meta::type_list<std::tuple<Okay, Okay2>, Errors..., Errors2...>::unique::template apply<Result>
  >
  {
    typename decltype(*this, next_res)::var_t new_variant;
    using result::utils::lift_into;
    if (*this) {
      if (next_res) {
        new_variant = detail::Wrapper<std::tuple<Okay, Okay2>>{
            std::make_tuple(this->unsafe_get_result(), next_res.unsafe_get_result())};
      } else {
        lift_into(std::move(next_res.to_variant()), new_variant, meta::type_list<Errors2...> {});
      }
    } else {
        lift_into(std::move(m_result), new_variant, meta::type_list<Errors...> {});
    }
    return new_variant;
  }


  template<typename Okay2, typename... Errors2>
  auto operator,(Result<Okay2, Errors2...>& next_res) -> std::enable_if_t<
    is_tuple<Okay>::value,
    typename meta::type_list<
      decltype(std::tuple_cat(this->unsafe_get_result(), std::make_tuple(next_res.unsafe_get_result()))),
      Errors..., Errors2...
    >::unique::template apply<Result>
  >
  {
    typename decltype(*this, next_res)::var_t new_variant;
    using result::utils::lift_into;
    if (*this) {
      if (next_res) {
        new_variant = typename decltype(*this, next_res)::okay_t {
          std::tuple_cat(this->unsafe_get_result(), std::make_tuple(next_res.unsafe_get_result()))
        };
      } else {
        lift_into(std::move(next_res.to_variant()), new_variant, meta::type_list<Errors2...> {});
      }
    } else {
        lift_into(std::move(m_result), new_variant, meta::type_list<Errors...> {});
    }
    return new_variant;
  }

  private:
  var_t m_result;
};


/*
 * Result<void, Okay> can be constructed, but you can never call then on it
 */
template <typename ...Errors>
struct Result<void, Errors...> : Result<detail::Wrapper<void>, Errors...> {
  using Result<detail::Wrapper<void>, Errors...>::Result;

  template<typename T>
  auto then(T) -> void {
    static_assert(std::is_same_v<T, void> && false, "You cannot continue from a void Result");
  }
};


template<typename F, typename Okay, typename... Errors>
auto operator|(F&& f, Result<Okay, Errors...> result) -> std::enable_if_t<
     is_tuple<Okay>::value
  && std::is_invocable_v<decltype(std::apply<F, Okay>), F, Okay>
  && not is_result<std::invoke_result_t<decltype(std::apply<F, Okay>), F, Okay>>::value,
  Result<std::invoke_result_t<decltype(std::apply<F, Okay>), F, Okay>, Errors...>
>
{
  using ret_type = decltype(f | result);
  typename ret_type::var_t new_variant;
  using result::utils::lift_into;
  if(result) {
    if constexpr(not std::is_same_v<decltype(std::apply(f, result.unsafe_get_result())), void>) {
      new_variant = typename ret_type::okay_t { std::apply(f, result.unsafe_get_result()) };
    } else {
      // only call the function for void, the default constructed value is fine
      std::apply(f, result.unsafe_get_result()) ;
    }
  } else {
    lift_into(result.to_variant(), new_variant, meta::type_list<Errors...> {});
  }
  return new_variant;
}

template<typename F, typename Okay, typename... Errors>
auto operator|(F&& f, Result<Okay, Errors...> result) -> std::enable_if_t<
     is_tuple<Okay>::value
  && std::is_invocable_v<decltype(std::apply<F, Okay>), F, Okay>
  && is_result<std::invoke_result_t<decltype(std::apply<F, Okay>), F, Okay>>::value, 
  typename std::invoke_result_t<decltype(std::apply<F, Okay>), F, Okay>::err_t::template prepend<
    typename std::invoke_result_t<decltype(std::apply<F, Okay>), F, Okay>::okay_unwrapped_t,
  Errors...>::template apply<Result>
>
{
  using ret_type = decltype(f | result);
  typename ret_type::var_t new_variant;
  using result::utils::lift_into;
  if(result) {
    lift_into(std::apply(f, result.unsafe_get_result()).to_variant(), new_variant);
  } else {
    lift_into(result.to_variant(), new_variant, meta::type_list<Errors...> {});
  }
  return new_variant;
}


}
